﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(MapBuilder))]
public class MapBuilderEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Generate"))
        {
            MapBuilder t = target as MapBuilder;
            t.CreateMap();
        }
    }

}
