﻿using UnityEngine;
using System.Collections;

public class SplashScreenScript : MonoBehaviour
{

    AsyncOperation async;

    void Start()
    {
        StartCoroutine(load());
        PlayService.Instance.Initialize();
    }

    void Update()
    {
        if (PlayService.Instance.Initialized)
            async.allowSceneActivation = true;
    }

    IEnumerator load()
    {
        Debug.Log("Load async started");
        async = Application.LoadLevelAsync("World1");
        async.allowSceneActivation = false;
        yield return async;
    }
}
