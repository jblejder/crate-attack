﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VersionShow : MonoBehaviour {

    public Text text;

	// Use this for initialization
	void Start () {
        text.text = "Version: " + Application.version;
	}
}
