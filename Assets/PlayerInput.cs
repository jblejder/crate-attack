﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class PlayerInput : Singleton<PlayerInput>
{
    public enum ControllType { Swipe, Tap };

    public float jumpTreshold;
    public float moveTreshold;

    public ControllType controllType;

    private Vector2 touchDif = Vector2.zero;
    private Vector2 touchReady = Vector2.zero;

    private bool touchStarted = false;
    private bool inputReady = false;

    private TilePlayerScript.MoveDirections tapRead;

    private static float _jumpTreshold;
    private static float _moveTreshold;

    void Start()
    {
        _jumpTreshold = jumpTreshold;
        _moveTreshold = moveTreshold;
    }

    void Update()
    {
        SwipeCalculations();
    }

    public TilePlayerScript.MoveDirections TapCalculations()
    {
        if (Application.isMobilePlatform)
        {
            if (Input.GetMouseButton(0))
            {
                Vector3 mp = Input.mousePosition;
                int vCenter = Screen.height / 2;
                int hCenter = Screen.width / 2;

                if (mp.x < hCenter && mp.y > vCenter)
                {
                    return TilePlayerScript.MoveDirections.leftJump;
                }
                if (mp.x > hCenter && mp.y > vCenter)
                {
                    return TilePlayerScript.MoveDirections.rightJump;
                }
                else if (mp.x < hCenter && mp.y < vCenter)
                {
                    return TilePlayerScript.MoveDirections.left;
                }
                else if (mp.x > hCenter && mp.y < vCenter)
                {
                    return TilePlayerScript.MoveDirections.right;
                }
            }
        }
        else
        {
            if (Input.GetKey("q"))
            {
                return TilePlayerScript.MoveDirections.leftJump;
            }
            else if (Input.GetKey("e"))
            {
                return TilePlayerScript.MoveDirections.rightJump;
            }
            else if (Input.GetKey("a"))
            {
                return TilePlayerScript.MoveDirections.left;
            }
            else if (Input.GetKey("d"))
            {
                return TilePlayerScript.MoveDirections.right;
            }
        }

        return TilePlayerScript.MoveDirections.none;
    }

    public TilePlayerScript.MoveDirections TapCalculationsDown()
    {
        if (Application.isMobilePlatform)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 mp = Input.mousePosition;
                int vCenter = Screen.height / 2;
                int hCenter = Screen.width / 2;

                if (mp.x < hCenter && mp.y > vCenter)
                {
                    return TilePlayerScript.MoveDirections.leftJump;
                }
                if (mp.x > hCenter && mp.y > vCenter)
                {
                    return TilePlayerScript.MoveDirections.rightJump;
                }
                else if (mp.x < hCenter && mp.y < vCenter)
                {
                    return TilePlayerScript.MoveDirections.left;
                }
                else if (mp.x > hCenter && mp.y < vCenter)
                {
                    return TilePlayerScript.MoveDirections.right;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown("q"))
            {
                return TilePlayerScript.MoveDirections.leftJump;
            }
            else if (Input.GetKeyDown("e"))
            {
                return TilePlayerScript.MoveDirections.rightJump;
            }
            else if (Input.GetKeyDown("a"))
            {
                return TilePlayerScript.MoveDirections.left;
            }
            else if (Input.GetKeyDown("d"))
            {
                return TilePlayerScript.MoveDirections.right;
            }
        }

        return TilePlayerScript.MoveDirections.none;
    }

    private void SwipeCalculations()
    {
        if (Input.touchCount > 0)
        {
            if (!touchStarted)
                touchDif = Vector2.zero;
            touchStarted = true;

            Touch touch = Input.GetTouch(0);
            touchDif += touch.deltaPosition;

        }
        else
        {
            if (touchStarted)
            {
                inputReady = true;
                touchReady = touchDif;
            }
            touchStarted = false;
        }
    }

    public TilePlayerScript.MoveDirections GetSwipeInput()
    {
        if (!inputReady)
            return TilePlayerScript.MoveDirections.none;

        inputReady = false;

        bool goRight;
        bool doJump = false;

        if (touchReady.x > moveTreshold)
            goRight = true;
        else if (touchReady.x < -moveTreshold)
            goRight = false;
        else
            return TilePlayerScript.MoveDirections.none;

        if (touchReady.y > jumpTreshold)
            doJump = true;

        if (doJump)
        {
            if (goRight)
                return TilePlayerScript.MoveDirections.rightJump;
            else
                return TilePlayerScript.MoveDirections.leftJump;
        }
        else
        {
            if (goRight)
                return TilePlayerScript.MoveDirections.right;
            else
                return TilePlayerScript.MoveDirections.left;
        }
    }

    public static TilePlayerScript.MoveDirections GetInput()
    {
        switch (Instance.controllType)
        {
            case ControllType.Swipe:
                return Instance.GetSwipeInput();
            case ControllType.Tap:
                return Instance.TapCalculations();
        }
        return TilePlayerScript.MoveDirections.none;
    }
}
