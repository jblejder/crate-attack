﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


[RequireComponent(typeof(Animator))]
public class Tutorial : MonoBehaviour
{

    public RectTransform textHolder;
    public LineRenderer verticalLine;
    public LineRenderer horizontalLine;

    public float length;
    public float textOffset;

    private Vector3 textStartPosition;
    private Animator animator;

    private bool playingAnim;

    void Start()
    {
        
        animator = GetComponent<Animator>();

        verticalLine.SetPosition(0, verticalLine.transform.position);
        verticalLine.SetPosition(1, verticalLine.transform.position);
        horizontalLine.SetPosition(0, horizontalLine.transform.position);
        horizontalLine.SetPosition(1, horizontalLine.transform.position);
        textStartPosition = textHolder.transform.position;

        GameManager.Instance.StartGameEvent += ShowTutorial;
    }

    void Update()
    {
        if (playingAnim)
        {
            verticalLine.SetPosition(1, verticalLine.transform.position + Vector3.up * length);
            horizontalLine.SetPosition(1, horizontalLine.transform.position + Vector3.right * length);
            textHolder.transform.position = textStartPosition + Vector3.up * textOffset;

        }
    }

    public void ShowTutorial()
    {
        Debug.Log("Wlaczono animacje");
        animator.SetTrigger("PlayTutorial");
        playingAnim = true;
        StartCoroutine(CheckIfEnd());
    }

    private IEnumerator CheckIfEnd()
    {
        yield return new WaitForSeconds(2);
        while (!animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            yield return null;

        playingAnim = false;
        Debug.Log("animacja wylaczona");
        GameManager.Instance.StartGameEvent -= ShowTutorial;
        gameObject.SetActive(false);

    }
}
