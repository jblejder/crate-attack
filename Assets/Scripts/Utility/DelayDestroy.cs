﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DelayDestroy : MonoBehaviour {

    public int second;
    private Image image;
	
	void Start () {
        image = GetComponent<Image>();
        image.enabled = true;
        Destroy(gameObject, second);
	}
}
