﻿using UnityEngine;
using System.Collections;


public class PlayerSpritesChanger : MonoBehaviour
{

    public TilePlayerScript player;
    public Sprite idle;
    public Sprite walkLeft;
    public Sprite walkRight;
    public Sprite jump;

    TilePlayerScript.States now = TilePlayerScript.States.idle;
    TilePlayerScript.MoveDirections dir;

    private SpriteRenderer s;

    void Start()
    {
        s = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (now != player.state)
        {
            now = player.state;
            s.sprite = GetSprite(now, player.dir);

        }
    }

    private Sprite GetSprite(TilePlayerScript.States state, TilePlayerScript.MoveDirections dir)
    {
        switch (state)
        {
            case TilePlayerScript.States.idle:
                return idle;
            case TilePlayerScript.States.jump:
                return jump;
            case TilePlayerScript.States.falling:
                return jump;
            case TilePlayerScript.States.moveing:
                if (dir == TilePlayerScript.MoveDirections.left)
                    return walkLeft;
                else
                    return walkRight;
            default:
                return idle;
        }
    }

}
