﻿using UnityEngine;
using System.Collections;


public class TileCheckers : MonoBehaviour
{

    public GameObject prefab;
    public TilePhysic tilephysic;
    GameObject checkers;

    void FixedUpdate()
    {
        if (checkers)
            Destroy(checkers);
        checkers = new GameObject("checkers");

        for (int x = 0; x < tilephysic.size_x; x++)
            for (int y = 0; y < tilephysic.size_y; y++)
            {
                if (TilePhysic.IsOnPosition(new TileVector(x, y)))
                {
                    GameObject c = Instantiate(prefab, new Vector3(x, y), Quaternion.identity) as GameObject;
                    c.transform.parent = checkers.transform;
                }
            }

    }
}
