﻿using UnityEngine;
using System.Collections;
using System;

public class TileBlock : TileObject
{

    public enum States { idle, falling, moveing };
    public States state = States.idle;
    public float pushSpeed = 3;

    private ParticleSystem particles;

    new void Start()
    {
        base.Start();
        particles = GetComponentInChildren<ParticleSystem>();
    }

    new void Update()
    {
        Vector3 targetX = targetPosition;
        targetX.y = transform.position.y;
        targetX = Vector3.MoveTowards(transform.position, targetX, pushSpeed * Time.deltaTime);

        Vector3 targetY = targetPosition;
        targetY.x = targetX.x;
        transform.position = Vector3.MoveTowards(targetX, targetY, speed * Time.deltaTime);

        


        if ((transform.position - targetPosition).sqrMagnitude < float.Epsilon)
        {
            state = States.idle;
        }
        if (state == States.idle)
        {
            if (!FallIfPossible())
                PlayerCrushed();
        }
    }
    public override void SetTileDestination(TileVector vec)
    {
        base.SetTileDestination(vec);
        if (state == States.idle)
            state = States.moveing;
    }

    private bool PlayerCrushed()
    {
        TileVector pos = Position;
        pos.y--;
        TileObject obj = TilePhysic.GetFromPosition(pos);
        if (obj != null)
        {
            if (typeof(TilePlayerScript).IsAssignableFrom(obj.GetType()))
            {
                TilePlayerScript player = obj as TilePlayerScript;
                GameManager.Instance.GameOver();
                return true;
            }
        }
        return false;
    }

    public bool FallIfPossible()
    {
        if (state != States.idle)
            return false;

        if (TilePhysic.SetFall(this))
        {
            state = States.falling;
            return true;
        }
        return false;
    }

    public override void Remove()
    {
        particles.transform.SetParent(null);
        particles.Play();
        SoundManager.Instance.CrateBreakSount();
        Destroy(gameObject);
        Destroy(particles, 2);
        
    }
}
