﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(TilePhysic))]
public class MapBuilder : MonoBehaviour
{

    public GameObject playerPrefab;
    public Sprite floorSprite;
    public Color wallColor;

    private TilePhysic p;

    void Start()
    {
        GameManager.Instance.mapBuilder = this;
    }

    public void CreateMap()
    {
        p = GetComponent<TilePhysic>();
        GameObject parent;
        if (parent = GameObject.Find("Map"))
            DestroyImmediate(parent);
        parent = new GameObject("Map");

        Debug.Log("MapBuilder przeładowany");
        TilePhysic.board = new TileObject[p.size_x, p.size_y];


        for (int x = 1; x <p.size_x - 1; x++)
        {
            TileObject o = TileObject.Create(new TileVector(x, 0), floorSprite, "floor");
            o.transform.parent = parent.transform;
        }
        for (int y = 0; y < p.size_y; y++)
        {
            GameObject o = TileObject.Create(new TileVector(0, y), wallColor, "wall").gameObject;
            o.transform.parent = parent.transform;
            o = TileObject.Create(new TileVector(p.size_x - 1, y), wallColor, "wall").gameObject;
            o.transform.parent = parent.transform;
        }

        int xPlayer = Random.Range(1, p.size_x-1);
        int yPlayer = Random.Range(1, p.size_y - 1);
        SpawnPlayer(xPlayer, yPlayer);
        ClearMap();

    }

    private void ClearMap()
    {
        GameObject root;
        if(root = GameObject.Find("Root"))
        {
            Destroy(root);
        }
    }

    void SpawnPlayer(int x, int y)
    {

        GameObject player = GameObject.Find("Player");
        if (player)
            DestroyImmediate(player);
        player = Instantiate(playerPrefab, new Vector3(x, y, 0), Quaternion.identity) as GameObject;
        player.name = "Player";
    }
}
