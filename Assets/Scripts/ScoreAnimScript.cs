﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode]
public class ScoreAnimScript : MonoBehaviour {

    public float textSize;
    public Text scoreText;
    public bool work;
	
	void Start () {
        textSize = scoreText.fontSize;
	}
    void Update()
    {
        if (work)
        {
            scoreText.fontSize = (int)textSize;
        }
    }
}
