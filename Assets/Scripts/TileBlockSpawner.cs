﻿using UnityEngine;
using System.Collections;

public class TileBlockSpawner : MonoBehaviour
{
    public float moveSpeed;
    public int releaseScoreValue;
    public GameObject cratePrefab;
    public Sprite[] crateSprites;
    public bool useDefinedSeed = false;
    public int randomSeed;

    private TileVector[] movePoints;
    private Vector3 goTo;

    private int releaseAt;
    private int step = 1;
    private int i = 0;
    private static GameObject root;

    void Start()
    {
        movePoints = new TileVector[TilePhysic.p.size_x - 2];
        for (int x = 0; x < movePoints.Length; x++)
        {
            movePoints[x] = new TileVector(x + 1, TilePhysic.p.size_y);
        }
        goTo = movePoints[0];
        if (useDefinedSeed)
            Random.seed = randomSeed;
        releaseAt = Random.Range(0, movePoints.Length - 1);
    }


    void Update()
    {
        if (GameManager.Instance.pause)
            return;
        if (!root)
        {
            root = new GameObject("Root");
        }
        transform.position = Vector3.MoveTowards(transform.position, goTo, moveSpeed * Time.deltaTime);
        if ((transform.position - goTo).sqrMagnitude < float.Epsilon)
        {
            if (i == releaseAt)
                Release();
            if (i >= movePoints.Length - 1)
                step = -1;
            if (i <= 0)
                step = 1;
            i += step;
            goTo = movePoints[i];
        }
    }

    private void Release()
    {
        TileVector vec = movePoints[i] + new TileVector(0, -1);
        if (TilePhysic.IsOnPosition(vec))
        {
            do
            {
                releaseAt = Random.Range(0, movePoints.Length);
                vec = movePoints[releaseAt] + new TileVector(0, -1);
            } while (TilePhysic.IsOnPosition(vec));
            return;
        }
        vec = movePoints[i] + new TileVector(0, -1);
        GameObject go = Instantiate(cratePrefab, vec, Quaternion.identity) as GameObject;
        go.transform.parent = root.transform;
        go.GetComponent<SpriteRenderer>().sprite = crateSprites[Random.Range(0, crateSprites.Length - 1)];
        releaseAt = Random.Range(0, movePoints.Length);
        GameManager.Instance.AddScore(releaseScoreValue);
    }
}
