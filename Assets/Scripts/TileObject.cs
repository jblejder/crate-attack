﻿using UnityEngine;
using System.Collections;

//[RequireComponent(typeof(SpriteRenderer))]
public class TileObject : MonoBehaviour
{
    public Vector3 targetPosition;
    public float speed = 1.0f;

    private TileVector position;
    private static Sprite _tileSprite;
    private static Sprite TileSprite
    {
        get
        {
            if (_tileSprite == null)
                _tileSprite = Resources.Load<Sprite>("TheOnlyOneImage");
            return _tileSprite;
        }
    }

    public TileVector Position
    {
        get
        {
            return new TileVector(position.x, position.y);
        }
        set
        {
            position = value;
        }
    }

    public void Awake()
    {
        Position = transform.position;
        targetPosition = transform.position;
    }

    public void Start()
    {
        if(!TilePhysic.IsOnPosition(Position))
        TilePhysic.Add(this);
    }

    public void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
    }

    public void FixedUpdate()
    {

    }

    public virtual void SetTileDestination(TileVector vec)
    {
        TilePhysic.Remove(this);
        Position = vec;
        targetPosition = vec;
        TilePhysic.Add(this);
    }

    public void SetAndAddDestination(TileVector vec)
    {
        Position = vec;
        targetPosition = vec;
    }

    public static TileObject Create(TileVector vec, string name = "TileObject")
    {
        GameObject obj = new GameObject(name, new System.Type[] { typeof(TileObject) });
        TileObject to = obj.GetComponent<TileObject>();
        SpriteRenderer sr = obj.GetComponent<SpriteRenderer>();

        obj.transform.position = vec;
        to.SetAndAddDestination(vec);
        sr.sprite = TileSprite;
        return to;
    }

    public static TileObject Create(TileVector vec, Color color, string name = "TileObject")
    {
        GameObject obj = new GameObject(name, new System.Type[] { typeof(TileObject), typeof(SpriteRenderer) });
        TileObject to = obj.GetComponent<TileObject>();
        SpriteRenderer sr = obj.GetComponent<SpriteRenderer>();

        obj.transform.position = vec;
        to.SetAndAddDestination(vec);
        sr.sprite = TileSprite;

        sr.color = color;
        return to;
    }

    public static TileObject Create(TileVector vec, Sprite sprite, string name = "TileObject")
    {
        GameObject obj = new GameObject(name, new System.Type[] { typeof(TileObject),typeof(SpriteRenderer) });
        TileObject to = obj.GetComponent<TileObject>();
        SpriteRenderer sr = obj.GetComponent<SpriteRenderer>();

        obj.transform.position = vec;
        to.SetAndAddDestination(vec);
        sr.sprite = sprite;

        
        return to;
    }

    public virtual void Remove()
    {

    }
}
