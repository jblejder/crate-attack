﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GooglePlayGames;


public class PlayService : Singleton<PlayService>
{
    private int loginRejected;
    private const string loginRejecterKey = "LoginRejected";
    private bool logged;
    private bool initialized = false;
    public bool Initialized { get { return initialized; } }
    void Start()
    {

    }

    public void Initialize()
    {
        PlayGamesPlatform.Activate();
        GameManager.Instance.GameOverEvent += new GameManager.GameOverEventHandler(PublishHighscore);

        RunAppLogin();
        initialized = true;
    }

    public void RunAppLogin()
    {
        
        if (PlayerPrefs.HasKey(loginRejecterKey))
            loginRejected = PlayerPrefs.GetInt(loginRejecterKey);
        else
            loginRejected = 0;

        if (loginRejected == 0)
            Authenticate();
    }

    public void Authenticate()
    {
        if (SystemInfo.deviceType != DeviceType.Handheld)
            return;
        if (Social.localUser.authenticated)
            return;
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                string token = PlayGamesPlatform.Instance.GetToken();
                loginRejected = 0;
            }
            else
            {
                loginRejected = 1;

            }
            PlayerPrefs.SetInt(loginRejecterKey, loginRejected);
            PlayerPrefs.Save();
        });
    }

    public void ShowLeaderboard()
    {
        Authenticate();
        PublishHighscore();
        PlayGamesPlatform.Instance.ShowLeaderboardUI(GooglePlayConstants.leaderboard_best_players);
    }

    public void PublishHighscore()
    {
        int highscore = GameManager.Instance.highscore;
        Social.ReportScore(highscore, GooglePlayConstants.leaderboard_best_players, null);
    }
}
