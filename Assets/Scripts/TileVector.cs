﻿using UnityEngine;
using System.Collections;

public class TileVector
{

    public int x { get; set; }
    public int y { get; set; }

    public TileVector() { }
    public TileVector(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    public TileVector(TileVector vec)
    {
        this.x = vec.x;
        this.y = vec.y;
    }

    public static TileVector operator +(TileVector a, TileVector b)
    {
        return new TileVector(a.x + b.x, a.y + b.y);
    }

    public static implicit operator Vector3(TileVector vec)
    {
        return new Vector3(vec.x, vec.y);
    }

    public static implicit operator TileVector(Vector3 vec)
    {
        return new TileVector((int)vec.x, (int)vec.y);
    }

    public static bool operator ==(TileVector a, TileVector b)
    {
        if (a.x == b.x)
            if (a.y == b.y)
                return true;
        return false;
    }

    public static bool operator !=(TileVector a, TileVector b)
    {
        if (a.x != b.x)
            if (a.y != b.y)
                return true;
        return false;
    }
    public override bool Equals(object obj)
    {
        if (!typeof(TileVector).IsAssignableFrom(obj.GetType()))
            return false;
        TileVector vec = obj as TileVector;
        if (this.x == vec.x)
            if (this.y == vec.y)
                return true;
        return false;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return "x: " + x + " y: " + y;
    }
}
