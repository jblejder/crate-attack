﻿using System;
using UnityEngine;
using System.Collections.Generic;


public class TilePhysic : MonoBehaviour
{

    public int size_x;
    public int size_y;
    public static TilePhysic p;
    public static TileObject[,] board;
    public int scoreForLine;
    private bool clearingLine;

    public void Awake()
    {
        p = this;
        board = new TileObject[size_x, size_y];
    }

    public void FixedUpdate()
    {
        if (IsFullLine())
        {
            StartCoroutine(DestroyLine());
        }
    }

    public System.Collections.IEnumerator DestroyLine()
    {
        clearingLine = true;
        yield return new WaitForSeconds(0.5f);
        for (int x = 1; x < size_x - 1; x++)
        {
            TileVector vec = new TileVector(x, 1);
            TileObject obj = GetFromPosition(vec);
            Remove(obj);
            obj.Remove();
            GameManager.Instance.AddScore(scoreForLine / (size_x - 1));
            yield return new WaitForSeconds(0.1f);
        }
        clearingLine = false;

        scoreForLine = (int)(1.2f * scoreForLine);
    }

    private bool IsFullLine()
    {
        if (clearingLine)
            return false;
        for (int x = 1; x < size_x - 1; x++)
        {
            TileObject check = GetFromPosition(new TileVector(x, 1));
            if (check == null)
                return false;
            if (typeof(TilePlayerScript).IsAssignableFrom(check.GetType()))
                return false;
        }
        return true;
    }

    public static void Add(TileObject obj)
    {
        if (IsOnPosition(obj.Position))
            Debug.LogError("There is something on Tile " + obj.Position + " " + GetFromPosition(obj.Position));
        board[obj.Position.x, obj.Position.y] = obj;
    }

    public static void Remove(TileObject obj)
    {
        board[obj.Position.x, obj.Position.y] = null;
    }

    public static bool SetFall(TileObject obj)
    {
        TileVector vec = obj.Position;
        vec.y--;
        if ((!IsOnPosition(vec)))
        {
            obj.SetTileDestination(vec);
            return true;
        }
        return false;
    }

    public static bool IsOnPosition(TileVector vec)
    {
        if (board == null)
        {
            Debug.LogError("Board isn't initialized");
        }
        if (vec.x < 0 || vec.x >= p.size_x || vec.y < 0 || vec.y >= p.size_y)
            return true;
        if (board[vec.x, vec.y])
            return true;
        return false;
    }

    public static TileObject GetFromPosition(TileVector vec)
    {
        if (vec.x < 0 || vec.x >= p.size_x || vec.y < 0 || vec.y >= p.size_y)
        {
            Debug.LogError("Out of index = board");
            return null;
        }
        return board[vec.x, vec.y];
    }

    public bool TryToMove(TileObject obj, TileVector destination)
    {
        return false;
    }

    private static bool CanBePushed(TileObject obj, int dir)
    {

        TileVector beyondDestination = new TileVector(obj.Position);
        beyondDestination.x += dir;
        if (IsOnPosition(beyondDestination))
            return false;

        if (IsOnPosition(obj.Position + new TileVector(0, 1)))
            return false;

        return true;
    }

    internal static bool TryPushOrMove(TilePlayerScript obj, TilePlayerScript.MoveDirections dir)
    {
        int side;
        switch (dir)
        {
            case TilePlayerScript.MoveDirections.left:
                side = -1;
                break;
            case TilePlayerScript.MoveDirections.right:
                side = 1;
                break;
            default:
                side = 0;
                break;
        }
        TileVector destination = obj.Position;
        destination.x += side;
        if (!IsOnPosition(destination)) // nie ma nic w tym miejscu
        {
            obj.SetTileDestination(destination);
            return true;
        }
        else if (CanBePushed(GetFromPosition(destination), side))
        {
            TileVector beyondDestination = new TileVector(destination);
            beyondDestination.x += side;
            GetFromPosition(destination).SetTileDestination(beyondDestination);
            obj.SetTileDestination(destination);
            return true;
        }
        return false;
    }



    public static bool Jump(TilePlayerScript obj, TilePlayerScript.MoveDirections dir, out List<TileVector> jumpWay, out int jumpIndex)
    {
        int side;
        jumpIndex = 0;
        jumpWay = new List<TileVector>();
        switch (dir)
        {
            case TilePlayerScript.MoveDirections.leftJump:
                side = -1;
                break;
            case TilePlayerScript.MoveDirections.rightJump:
                side = 1;
                break;
            default:
                side = 0;
                break;
        }

        if (IsOnPosition(obj.Position + new TileVector(side, 0)))
        {
            Debug.Log("klocek obok");
            jumpWay.Add(obj.Position + new TileVector(0, 1));
            jumpWay.Add(obj.Position + new TileVector(side, 1));
            return true;
        }
        else
        {
            Debug.Log("brak klocka obok");
            jumpWay.Add(obj.Position + new TileVector(side, 1));
            if (IsOnPosition(obj.Position + new TileVector(2 * side, 0)))
            {
                jumpWay.Add(obj.Position + new TileVector(2 * side, 1));
            }
            return true;
        }
    }

    public static int JumpNext(TilePlayerScript obj, int jumpIndex, List<TileVector> jumpWay)
    {
        if (obj.Position.x == jumpWay[jumpIndex].x)
        {
            obj.SetTileDestination(jumpWay[jumpIndex]);
            return jumpIndex + 1;
        }
        else
        {
            int direction = jumpWay[jumpIndex].x - obj.Position.x;
            TileVector destination = new TileVector(jumpWay[jumpIndex]);
            if (!IsOnPosition(destination))
            {
                obj.SetTileDestination(destination);
                return jumpIndex + 1;
            }
            else if (CanBePushed(GetFromPosition(destination), direction))
            {
                TileVector beyondDestination = new TileVector(destination);
                beyondDestination.x += direction;
                GetFromPosition(destination).SetTileDestination(beyondDestination);
                obj.SetTileDestination(destination);
                return jumpIndex + 1;
            }
            return int.MaxValue;
        }
    }
}
