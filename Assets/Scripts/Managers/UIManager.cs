﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    public GameObject mainMenu;
    public Text score;
    public Text highscore;
    public Text changeControll;
    public int skipFrames;
    private int skipFramesCounter = 0;

    void Start()
    {
        GameManager.Instance.GameOverEvent += new GameManager.GameOverEventHandler(GameOver);
        Debug.Log("start");
        mainMenu.SetActive(true);
        score.enabled = false;
    }

    public void Update()
    {
        if (--skipFramesCounter < 0)
        {
            score.text = "Score\n" + GameManager.Instance.score;
            highscore.text = "Score\n" + GameManager.Instance.score + "\nBest\n" + GameManager.Instance.highscore;
            skipFramesCounter = skipFrames;
            if (mainMenu.activeInHierarchy)
            {
                SetChangeControllText();
            }
        }

    }

    void GameOver()
    {
        changeControll.text = PlayerInput.Instance.controllType.ToString() + "\ncontroll";
        score.enabled = false;
        mainMenu.SetActive(true);
    }

    public void PlayAgain()
    {
        GameManager.Instance.PlayAgain();
        score.enabled = true;
        mainMenu.SetActive(false);
    }

    public void ChangeControll()
    {
        changeControll.text = GameManager.Instance.ChangeControll() + "\ncontroll";
    }
    public void SetChangeControllText()
    {
        changeControll.text = PlayerInput.Instance.controllType.ToString() + "\ncontroll";
    }

    public void Quit()
    {
        Application.Quit();
    }
}