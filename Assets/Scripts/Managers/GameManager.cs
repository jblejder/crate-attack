﻿using UnityEngine;
using System.Collections;

public class GameManager : Singleton<GameManager>{

    public MapBuilder mapBuilder;
    public int score;// { get; private set; }
    public int highscore;// { get; private set; }
    public TilePlayerScript player;// { get; set; }
    public bool pause = false;

    public delegate void GameOverEventHandler();
    public event GameOverEventHandler GameOverEvent;

    public delegate void StartGameEventHandler();
    public event StartGameEventHandler StartGameEvent;

    public Tutorial tutorial;
    private bool firstStart;


	void Start () {
        highscore = PlayerPrefs.GetInt("highscore");
        PlayerInput.Instance.controllType = (PlayerInput.ControllType)PlayerPrefs.GetInt("controllType");
	}
	
    new void OnDestroy()
    {
        base.OnDestroy();
        PlayerPrefs.SetInt("highscore", highscore);
        PlayerPrefs.Save();
    }

    public void AddScore(int score)
    {
        if (pause || !firstStart)
            return;
        this.score += score;
        if (highscore < this.score)
        {
            highscore = this.score;
            PlayerPrefs.SetInt("highscore", highscore);
        }
    }

    public void GameOver()
    {
        if (!pause)
        {
            Debug.Log("GameOver");
            Social.ReportScore(highscore, GooglePlayConstants.leaderboard_best_players, (bool succes) => { });
            PlayerPrefs.Save();
            if (GameOverEvent != null)
            {
                GameOverEvent();
            }
            pause = true;
        }
    }

    public void PlayAgain()
    {
        if (StartGameEvent != null)
            StartGameEvent();
        score = 0;
        mapBuilder.CreateMap();
        pause = false;
        firstStart = true;
    }

    public void ShowLeaderBoard()
    {
        Social.ShowLeaderboardUI();
    }

    public string ChangeControll()
    {
        if (PlayerInput.Instance.controllType == PlayerInput.ControllType.Swipe)
            PlayerInput.Instance.controllType = PlayerInput.ControllType.Tap;
        else
            PlayerInput.Instance.controllType = PlayerInput.ControllType.Swipe;

        PlayerPrefs.SetInt("controllType", (int)PlayerInput.Instance.controllType);
        PlayerPrefs.Save();

        return PlayerInput.Instance.controllType.ToString();
    }
}
