﻿using UnityEngine;
using System.Collections;

public class SoundManager : Singleton<SoundManager>
{

    public AudioSource audioSource;
    public AudioClip [] crateBreak;
    public bool disableAudiio;
    public void CrateBreakSount()
    {
        if (disableAudiio)
            return;
        audioSource.clip = crateBreak[(int)Random.Range(0, crateBreak.Length)];
        audioSource.Play();
    }

}
