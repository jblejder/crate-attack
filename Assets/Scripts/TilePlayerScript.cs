﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class TilePlayerScript : TileObject
{
    public enum States { idle, falling, moveing, jump };
    public enum MoveDirections { left, leftJump, jump, rightJump, right, none };

    public States state = States.idle;
    [Range(0, 1)]
    public float inputDistanceTreshold;
    public int maxInputQueue;

    private List<TileVector> jumpWay;
    private int jumpIndex;
    private Queue<MoveDirections> InputQueue;


    [HideInInspector]
    public MoveDirections dir = MoveDirections.none;
    private bool blockInput;

    new void Start()
    {
        base.Start();
        GameManager.Instance.player = this;
    }

    new void Update()
    {
        base.Update();

        float squarePositionDistance = (transform.position - targetPosition).sqrMagnitude;
        if (squarePositionDistance < float.Epsilon)
        {
            if (state != States.jump)
                state = States.idle;
            else
            {
                JumpNext();
            }
        }
        FallIfPossible();
        if (GameManager.Instance.pause)
            return;

        dir = PlayerInput.GetInput();

        if (state == States.idle)
        {
            TryToMove(dir);
        }
    }

    private void JumpNext()
    {
        if (jumpIndex >= jumpWay.Count)
        {
            state = States.idle;
            return;
        }
        else
        {
            jumpIndex = TilePhysic.JumpNext(this, jumpIndex, jumpWay);
        }
    }

    public void FallIfPossible()
    {
        if (state != States.idle)
            return;

        if (TilePhysic.SetFall(this))
        {
            state = States.falling;
        }
    }

    private void TryToMove(MoveDirections dir)
    {
        bool moving = false;
        bool jumping = false;
        switch (dir)
        {
            case MoveDirections.none:
                return;
            case MoveDirections.left:
                moving = TilePhysic.TryPushOrMove(this, dir);
                break;
            case MoveDirections.right:
                moving = TilePhysic.TryPushOrMove(this, dir);
                break;
            case MoveDirections.leftJump:
                jumping = TilePhysic.Jump(this, dir, out jumpWay, out jumpIndex);
                break;
            case MoveDirections.rightJump:
                jumping = TilePhysic.Jump(this, dir, out jumpWay, out jumpIndex);
                break;
        }
        if (moving)
            state = States.moveing;
        else if (jumping)
            state = States.jump;
    }
}
